//
//  TwitterAPIRequestDelegate.swift
//  socialtweet
//
//  Created by Ismael Terreno on 12/26/15.
//  Copyright © 2015 Ismael Terreno. All rights reserved.
//

import Foundation

protocol TwitterAPIRequestDelegate {
    
    func handleTwitterData (data: NSData!,
        urlResponse: NSHTTPURLResponse!,
        error: NSError!,
        fromRequest: TwitterAPIRequest!)
}