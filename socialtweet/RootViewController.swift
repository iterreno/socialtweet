//
//  ViewController.swift
//  socialtweet
//
//  Created by Ismael Terreno on 12/3/15.
//  Copyright © 2015 Ismael Terreno. All rights reserved.
//

import UIKit
import Social
import Accounts
import Photos
import CoreImage


public class RootViewController: UITableViewController , TwitterAPIRequestDelegate {
    
    var parsedTweets : Array <ParsedTweet> = [];
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        reloadTweets();
        let refresher = UIRefreshControl();
            refresher.addTarget(self,
            action: "handleRefresh:",
            forControlEvents: UIControlEvents.ValueChanged);
        refreshControl = refresher;
    
    }

    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override public func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1;
    }
    
    override public func tableView(_tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Section tweets";
    }
    
    override public func tableView(_tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return parsedTweets.count;
    }
    
    override public func tableView (_tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        // We lookup the custom cell to use it.
        let cell = tableView.dequeueReusableCellWithIdentifier("CustomTweetCell") as! ParsedTweetCell;
        let parsedTweet = parsedTweets[indexPath.row];
        cell.userNameLabel.text = parsedTweet.userName;
        cell.tweetTextLabel.text = parsedTweet.tweetText;
        cell.createdAtLabel.text = parsedTweet.createdAt;
        dispatch_async(dispatch_get_global_queue( QOS_CLASS_DEFAULT, 0) ,
            {
                if let imageData = NSData (contentsOfURL: parsedTweet.userAvatarURL!) {
                    let avatarImage = UIImage(data: imageData)
                    dispatch_async(dispatch_get_main_queue(),
                    {
                            if cell.userNameLabel.text == parsedTweet.userName {
                                cell.avatarImageView.image = avatarImage
                            }else {
                                print("oops, wrong cell, never mind")
                            }
                    })
                }
        })
        return cell;
    }
    
    /**
     OAuth 2.0 to Grant access for the Tweeter API.
    */
    func reloadTweets() {
        let twitterParams : Dictionary = ["count":"100"];
        let twitterAPIURL = NSURL(string:
            "https://api.twitter.com/1.1/statuses/home_timeline.json");
        let request = TwitterAPIRequest();
        request.sendTwitterRequest(
                twitterAPIURL,
                params: twitterParams,
                delegate: self);
    }
    
    /**
     Callback to Handle Tweeter data
    */
    func handleTwitterData (data: NSData!, urlResponse: NSHTTPURLResponse!, error: NSError!, fromRequest: TwitterAPIRequest!) {
        if let dataValue = data {
            do{
                let jsonObject : AnyObject? = try NSJSONSerialization.JSONObjectWithData(dataValue,
                    options: NSJSONReadingOptions(rawValue: 0));
                    if let jsonArray = jsonObject as? [[String:AnyObject]] {
                        self.parsedTweets.removeAll(keepCapacity: true);
                        for tweetDict in jsonArray {
                            let parsedTweet = ParsedTweet();
                            parsedTweet.tweetIdString = tweetDict["id_str"] as? String;
                            parsedTweet.tweetText = tweetDict["text"] as? String;
                            parsedTweet.createdAt = tweetDict["created_at"] as? String;
                            let userDict = tweetDict["user"] as! NSDictionary;
                            parsedTweet.userName = userDict["name"] as? String;
                            // Here I change the shceme to a secure https because apple 
                            // wants to developers use ssl when make requests.
                            let secureURL = StringUtils().changeSecureSchemeURL(userDict["profile_image_url"] as! String);
                            parsedTweet.userAvatarURL = NSURL (string: secureURL);
                            self.parsedTweets.append(parsedTweet);
                        }
                        // This dispatch the thread to the main queue to give quick aviability to the UI
                        // while the http service is still runing. So give us more sense of speed.
                        dispatch_async( dispatch_get_main_queue(),
                            {
                                self.tableView.reloadData()
                            })
                    }
            } catch {
                print ("Exeption when received data");
            }
            
        } else {
                print ("handleTwitterData received no data");
        }
    }
    
    /**
     Callback to Handle Pull gesture for the table view.
     */
    @IBAction func handleRefresh (sender : AnyObject?) {
        reloadTweets();
        refreshControl!.endRefreshing();
    }
    
    @IBAction func handleTweetButtonTapped(sender: AnyObject) {
        if SLComposeViewController.isAvailableForServiceType(SLServiceTypeTwitter){
            let tweetVC = SLComposeViewController (forServiceType: SLServiceTypeTwitter)
            tweetVC.setInitialText( "I just finished the first project in iOS 8 SDK Development. #pragsios8");
            presentViewController(tweetVC, animated: true, completion: nil);
        } else {
            print("Can't send tweet");
        }
        
    }
    
    override public func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showTweetDetailsSegue" {
            if let tweetDetailVC = segue.destinationViewController
                as? TweetDetailViewController {
                let row = tableView!.indexPathForSelectedRow!.row;
                let parsedTweet = parsedTweets [row] as ParsedTweet;
                tweetDetailVC.tweetIdString = parsedTweet.tweetIdString;
                print("tapped on: \(parsedTweet.tweetText!)");
            }
        }
    }
    
    override public func tableView(tableView: UITableView, didSelectRowAtIndexPath
        indexPath: NSIndexPath) {
        let parsedTweet = parsedTweets[indexPath.row]
        if self.splitViewController != nil {
            if (self.splitViewController!.viewControllers.count > 1) {
                if let tweetDetailNav = self.splitViewController!.viewControllers[1] as? UINavigationController {
                    if let tweetDetailVC = tweetDetailNav.viewControllers[0] as? TweetDetailViewController {
                        tweetDetailVC.tweetIdString = parsedTweet.tweetIdString
                    }
                }
            }
            tableView.deselectRowAtIndexPath(indexPath, animated: false)
        }
    }
    
    
    @IBAction func handlePhotoButtonTapped(sender: UIBarButtonItem) {
        let fetchOptions = PHFetchOptions();
        PHPhotoLibrary.requestAuthorization {
            (authorized: PHAuthorizationStatus) -> Void in
            if authorized == .Authorized {
                fetchOptions.sortDescriptors =
                    [NSSortDescriptor(key: "creationDate", ascending: false)]
                let fetchResult = PHAsset.fetchAssetsWithMediaType (
                    PHAssetMediaType.Image,
                    options: fetchOptions)
                if let firstPhoto = fetchResult.firstObject  as? PHAsset {
                    self.createTweetForAsset(firstPhoto)
                } 
            } 
        }
    }
    
    func createTweetForAsset (asset: PHAsset) {
        let requestOptions =  PHImageRequestOptions()
        requestOptions.synchronous = true
        
        PHImageManager.defaultManager().requestImageForAsset(asset,
            targetSize: CGSizeMake(640, 480),
            contentMode: PHImageContentMode.AspectFit,
            options: requestOptions) {
                (image:UIImage?, info: [NSObject : AnyObject]?) -> Void in
                // Beginning of changes
                var ciImage = CIImage(image: image!)
                ciImage = ciImage!.imageByApplyingFilter("CIPixellate",
                    withInputParameters:
                    ["inputScale" : 15,
                    ])
                let ciContext = CIContext(options:nil)
                let cgImage = ciContext.createCGImage (ciImage!, fromRect: (ciImage?.extent)!)
                let tweetImage = UIImage (CGImage: cgImage)
                // End of Changes
                let tweetVC = SLComposeViewController (forServiceType:
                    SLServiceTypeTwitter)
                let message = NSLocalizedString(
                    "Here's a photo I tweeted. #pragsios8",
                    comment:"")
                tweetVC.setInitialText(message)
                tweetVC.addImage(tweetImage)
                
                dispatch_async(dispatch_get_main_queue(), {
                    self.presentViewController(tweetVC, animated: true, completion: nil)
                }) 
        } 
    }
    
    
    
}

