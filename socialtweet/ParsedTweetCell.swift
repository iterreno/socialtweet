//
//  ParsedTweetCell.swift
//  socialtweet
//
//  Created by Ismael Terreno on 12/17/15.
//  Copyright © 2015 Ismael Terreno. All rights reserved.
//

import UIKit

class ParsedTweetCell: UITableViewCell {

    @IBOutlet weak var avatarImageView: UIImageView!;
    @IBOutlet weak var tweetTextLabel: UILabel!;
    @IBOutlet weak var createdAtLabel: UILabel!;
    @IBOutlet weak var userNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
