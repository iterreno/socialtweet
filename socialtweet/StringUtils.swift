//
//  StringUtils.swift
//  socialtweet
//
//  Created by Ismael Terreno on 12/30/15.
//  Copyright © 2015 Ismael Terreno. All rights reserved.
//

import Foundation

public class StringUtils {


    /*
    This changes the http shceme to a secure https.
    */
    public func changeSecureSchemeURL( urlToProcess:String) -> String{
        let secureURLresult = urlToProcess.stringByReplacingOccurrencesOfString("http:", withString: "https:", options: NSStringCompareOptions.LiteralSearch, range: nil);
        return secureURLresult;
    }
    
}