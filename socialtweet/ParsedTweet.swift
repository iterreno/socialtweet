//
//  ParsedTweet.swift
//  socialtweet
//
//  Created by Ismael Terreno on 12/17/15.
//  Copyright © 2015 Ismael Terreno. All rights reserved.
//

import UIKit

let defaultAvatarURL = NSURL(string: "https://abs.twimg.com/sticky/default_profile_images/" + "default_profile_6_200x200.png");

public class ParsedTweet: NSObject {
    var tweetText : String?;
    var userName : String?;
    var createdAt: String?;
    var userAvatarURL : NSURL?;
    var tweetIdString : String?;
    
    override init () {
        super.init()
    }
    
    init (tweetText: String?,
        userName: String?,
        createdAt: String?,
        userAvatarURL : NSURL?) {
        super.init()
        self.tweetText = tweetText;
        self.userName = userName;
        self.createdAt = createdAt;
        self.userAvatarURL = userAvatarURL ;
    }
    
    
}
