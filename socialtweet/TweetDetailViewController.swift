//
//  TweetDetailViewController.swift
//  socialtweet
//
//  Created by Ismael Terreno on 12/27/15.
//  Copyright © 2015 Ismael Terreno. All rights reserved.
//

import UIKit

class TweetDetailViewController: UIViewController , TwitterAPIRequestDelegate {
    
    @IBOutlet weak var userImageButton: UIButton!
    @IBOutlet weak var userRealNameLabel: UILabel!
    @IBOutlet weak var userScreenNameLabel: UILabel!
    @IBOutlet weak var tweetTextLabel: UILabel!
    @IBOutlet weak var tweetImageView: UIImageView!
    
    
    
    
    var tweetIdString : String? {
        didSet {
            reloadTweetDetails();
        }
    };
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        reloadTweetDetails()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func reloadTweetDetails() {
        if tweetIdString == nil {
            return;
        }
        let twitterRequest = TwitterAPIRequest();
        let twitterParams = ["id" : tweetIdString!];
        let twitterAPIURL = NSURL (string: "https://api.twitter.com/1.1/statuses/show.json");
        twitterRequest.sendTwitterRequest(
            twitterAPIURL,
            params: twitterParams,
            delegate: self
        );
    }
    
    func handleTwitterData (data: NSData!,
                urlResponse: NSHTTPURLResponse!,
                error: NSError!,
                fromRequest: TwitterAPIRequest!){
        
        if let dataValue = data {
            do{
                let jsonObject : AnyObject? = try NSJSONSerialization.JSONObjectWithData(dataValue,
                    options: NSJSONReadingOptions(rawValue: 0));
                    if let tweetDict = jsonObject as? [String:AnyObject] {
                        dispatch_async(dispatch_get_main_queue(), {
                            print("\(tweetDict)");
                            let userDict = tweetDict["user"] as! NSDictionary
                            self.userRealNameLabel.text = userDict["name"] as? String
                            self.userScreenNameLabel.text = userDict["screen_name"] as? String
                            self.tweetTextLabel.text = tweetDict["text"] as? String
                            let safeURL = StringUtils().changeSecureSchemeURL(userDict["profile_image_url"] as!String );
                            let userImageURL = NSURL (string: safeURL)
                            self.userImageButton.setTitle(nil, forState: .Normal)
                            if userImageURL != nil {
                                if let imageData = NSData(contentsOfURL: userImageURL!) {
                                self.userImageButton.setImage(
                                    UIImage(data: imageData),
                                    forState: UIControlState.Normal
                                );
                                }
                            }
                            // If the selected tweet has an image that was uploaded with Twitter’s own image- hosting service. Just show it.
                            if let entities = tweetDict["entities"] as? NSDictionary {
                                    if let media = entities ["media"] as? NSArray {
                                        if let mediaString = media[0]["media_url"] as? String {
                                            if let mediaURL = NSURL(string: StringUtils().changeSecureSchemeURL(mediaString)) {
                                                if let mediaData = NSData (contentsOfURL: mediaURL) {
                                                    self.tweetImageView.image = UIImage(data: mediaData)
                                                }
                                            }
                                        }
                                    }
                            }
                        })
                    }
            } catch {
                        print ("Exeption when received data");
            }
        } else {
            print ("handleTwitterData received no data");
        }
    }
    
    @IBAction func unwindToTweetDetailVC (segue: UIStoryboardSegue?) { }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "showUserDetailsSegue") {
            if let userDetailVC = segue.destinationViewController as? UserDetailViewController {
                userDetailVC.screenName = userScreenNameLabel.text
            }
        }
    }

}
